"""
#Escriba un ciclo WHILE que lea números hasta que el usuario lo desee y los agregue a un
#diccionario, posteriormente imprima los elementos de dicho diccionario, el key del
#diccionario de ser generado aleatoriamente (Tema: ciclo While, Diccionario) 4 puntos

diccionario={}
num=0
num=int(input("Ingrese los valores que desea al diccionario: "))
opcion='s'

while num!=0:
    print(num)
    num=num+1
    usu = input("Desea continuar:")

else:
    print("Gracias por usar el sistema.")
"""
"""
#Cree una matriz de 3 x 3 y rellene los elementos con números aleatorios según se indica

#• La columna #1 debe tener números entre 10 y 20
#• La columna #2 debe tener números entre 21 y 30
#• La columna #3 debe tener números entre 31 y 40
#• La posición central debe ser un 99

import random as rd
filas=3
columnas=3


matriz=[]
for i in range(filas):
    matriz.append([0] * columnas)
for i in range(filas):
    for j in range(columnas):
        col1 = rd.randrange(10, 20)

        if j==0:
            matriz[i][j] =col1
    for m in range(columnas):
        col2 = rd.randrange(21, 30)
        if m==99:
            matriz[i][m]=col2
    for h in range(columnas):
        col3 = rd.randrange(31, 40)
        if h==0:
            matriz[i][h]=col3
for i in matriz:
    print(i)

"""



#Al ejercicio anterior, generarle un ciclo de manera que el usuario pueda imprimir las
#matrices diferentes hasta que lo desee

#Crear la CLASE vehículo con atributos (color, marca, y precio) y realizar un proceso que
#permita guardar 3 automóviles en una lista
#Al terminar de agregar los elementos, debe poder recorrer la lista y preguntarle al usuario
#si desea cambiar el precio del vehículo, en caso afirmativo realizar el cambio respectivo y
#continuar imprimiendo hasta terminar con los demás objetos de la lista (tema: clases,
#objetos, listas, ciclos)
lista=list()
num=0
class Vehiculo:
    def __init__(self,v_color,v_marca,v_precio):
        self.color=v_color
        self.marca=v_marca
        self.precio=v_precio


opcion=1
while opcion<=3:
    color=input("Ingrese el color del vehiculo: ")
    marca =input("Ingrese el marca del vehiculo: ")
    precio =int(input("Ingrese el precio del vehiculo: "))
    vehiculo=Vehiculo(color,marca,precio)
    lista.append(Vehiculo)
    opcion=opcion+1

print("Los automoviles ingresados son: " ,color,marca,precio)


